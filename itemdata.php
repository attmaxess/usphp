<?php
	$servername = "localhost";
	$username = "root";
	$password = "123456";
	$dbName = "ubersport"; 
	
	//Make Connection
	$conn = new mysqli($servername, $username, $password, $dbName);
	//Check Connection
	if (!$conn){
		die("Connection Failed. ". mysqli_connect_error());		
	} //else echo "connection ok";

	$sql = "SELECT name, type FROM items";
	$result = mysqli_query($conn, $sql);

	//if (!$result) echo "no result";
	//else echo "have result".strval(mysqli_num_rows($result))."rows";

	$items = array();
	
	if (mysqli_num_rows($result) > 0) {
		//show data for each row		
		
		while($row = mysqli_fetch_assoc($result)) {
			$items[] = array('name' => $row['name'], 'type' => $row['type']);			
		}	
		
	}
	
	$object = (object) $items;
	header('Content-Type: application/json');
	//echo "{\"item_array\":" . json_encode($items) ."}";	
	//echo "{\"item_string\":" . json_encode($object)."}";
	echo json_encode($object);

?>